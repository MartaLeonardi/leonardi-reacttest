import { createSlice } from '@reduxjs/toolkit';

// Define the initial state using that type
const initialState = {
  value: 0,
  value2:null
};

export const counterSlice = createSlice({
  name: 'counter',
  // `createSlice` will infer the state type from the `initialState` argument
  initialState,
  reducers: {
    increment: (state) => {
      state.value += 1;
    },
    decrement: (state) => {
      state.value -= 1;
    },
    // Use the PayloadAction type to declare the contents of `action.payload`
    incrementByAmount: (state, action) => {
        state.value += action.payload;
      },
    reset: (state) =>{
        state.value = 0;
    },

    setUser: (state, action) =>{
        state.value2=action.payload
    }
    },
  });
  
  export const { increment, decrement, incrementByAmount, reset, setUser} = counterSlice.actions;