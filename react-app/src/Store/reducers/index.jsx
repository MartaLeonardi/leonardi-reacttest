import { combineReducers } from 'redux';
import { counterSlice } from './esempio';
import {loginSlice} from './login';

export default combineReducers({

    esempio: counterSlice.reducer,
    login: loginSlice.reducer
});

