import { createSlice } from '@reduxjs/toolkit';

// Define the initial state using that type
const initialState = {
  token: "",
  ttl : "",
  tokenCreationTime :""
};


export const loginSlice = createSlice({
  name: 'datiLogin',
  
  initialState,
  reducers: {
        setToken: (state, action) =>{
            state.token = action.payload
        },
        setTtl: (state, action) =>{
            state.ttl=action.payload
        },
        setTokenCreationTime : (state, action) =>{
            state.tokenCreationTime=action.payload
        },
        reset : (state, action)=>{
            state.token ="",
            state.ttl="",
            state.tokenCreationTime=""
        }
    },
  });
  
  export const { setToken, setTtl, setTokenCreationTime, reset } = loginSlice.actions;