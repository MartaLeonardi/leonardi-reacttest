import { useState } from "react"
import { AuthContext } from "./AuthContext"


export default function AuthContextProvider ({children}){

    const [datiForm, setDatiForm] = useState({
        nome: "",
        cognome:"",
        email:"",
        password:""
    })

    return(
        <>
        <AuthContext.Provider value={{datiForm, setDatiForm}}>
                {children}
        </AuthContext.Provider>
        </>
    )

}