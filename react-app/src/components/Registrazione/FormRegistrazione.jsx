import { useState } from "react"
import "../Registrazione/FormRegistrazione.css"
import { useNavigate } from "react-router-dom"
import AuthContextProvider from "../../contexts/AuthContextProvider"
import { useContext } from "react"
import { AuthContext } from "../../contexts/AuthContext"

export default function FormRegistrazione() {

//abilito la navigazione ad una root definita
const navigateTo = useNavigate()


//oggetto che restituirà
const [datiUtente, setDatiUtente] = useState([])

//gestione dell'evento per l'inserimento dei dati all'interno dei campi
const handleChange = (e) => {
    const {name, value} = e.target
    //setta il valore inserito in un campo nel form, all'interno dell'oggetto che conterrà tutti i dati (formData)
    setDatiForm({...datiForm, [name]: value})
}



const {datiForm, setDatiForm} = useContext(AuthContext)


//gestione evento per l'invio dei dati
const handleSubmit=(e) => {
    e.preventDefault();
    console.log(datiForm);

//controllo del pattern
if( !(generalitaValida(datiForm.nome) || generalitaValida(datiForm.cognome))){
    alert("Generalità inserite non corrette");
    return;
}else if(!(mailValida(datiForm.email))){
    alert("Pattern email non corretto");
    return;
}else if(!(passwordValida(datiForm.password))){
    alert("Pattern password non corretta!\nRichiede:\n-Lettera maiuscola\n-Almeno un numero\n-Un simbolo speciale (ad esempio $)");
    return;
}


    console.log(JSON.stringify(datiForm));
    //setta i valori presi dal form, nell'oggetto che verrà restituito
    setDatiUtente(datiForm)

    setDatiForm(datiForm)

    //invio dei dati al backend
    fetchDataPost();
    
}

//fetch dati post
const fetchDataPost = async () =>{
    const response = await fetch("http://localhost:8080/api/utente/registrazione", {
        mode: "cors",
        method: "POST",
        headers:{
            'Content-Type': 'application/json',
          },
        body: JSON.stringify(datiForm),
    });

  console.log(response.status)

  if(response.status == 200){
    alert("Registrazione avvenuta! Effettua il login per autenticarti");

    //collegamento con il profilo utente
    navigateTo("/login");

    //faccio il return dell'oggetto creato
    return JSON.stringify(datiForm);
  }else{
    alert("Si è verificato un errore!");
    //ritorna lo stato del metodo post
    return response.status;
  }
  

}


//controllo del pattern del nome e cognome
const generalitaValida = (generalita) =>{

    const regexGeneralita= /^[A-Za-zÀ-Ùà-ù][a-zà-ù]*$/;
    console.log(regexGeneralita.test(generalita));
    return regexGeneralita.test(generalita);

}

//controllo pattern mail
const mailValida = (email) =>{
    const regexMail=/[A-z0-9.+_-]+@[A-z0-9._-]+.[A-z]{2,8}/;
    console.log(regexMail.test(email));
    return regexMail.test(email);
}

//controllo del pattern della password, e lunghezza
const passwordValida = (password) =>{
    const regexPassword =/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@.#$!%*?&^])[A-Za-z\d@.#$!%*?&]{6,20}$/;
    console.log(regexPassword.test(password))
    return regexPassword.test(password)
}





    return(
        <>

        <div id="formRegistrazione" className="container-sm">
            <h1 id="intestazione">EFFETTUA QUI LA TUA REGISTRAZIONE</h1>
            
            <form onSubmit={handleSubmit}>
                
                <div className="container text-center">
                <div className="row">
                    <div className="col-6 col-sm-6">
                        <label className="form-label">Nome</label>
                    </div>
                    <div className="col-6 col-sm-6">
                        <input type="text" className="form-control" name="nome" placeholder="Nome" onChange={handleChange} value={datiForm.nome} required/>
                    </div>

                        <div className="w-100 d-none d-md-block" id="separatore"></div>

                    <div className="col-6 col-sm-6">
                        <label className="form-label">Cognome</label>
                    </div>
                    <div className="col-6 col-sm-6">
                        <input type="text" className="form-control" name="cognome" placeholder="Cognome" onChange={handleChange} value={datiForm.cognome} required/>
                    </div>

                        <div className="w-100 d-none d-md-block" id="separatore"></div>

                    <div className="col-6 col-sm-6">
                        <label className="form-label">Email</label>
                    </div>
                    <div className="col-6 col-sm-6">
                        <input type="email" className="form-control" name="email" placeholder="Email" onChange={handleChange} value={datiForm.email} required/>
                    </div>

                        <div className="w-100 d-none d-md-block" id="separatore"></div>

                    <div className="col-6 col-sm-6">
                        <label className="form-label">Password</label>
                    </div>
                    <div className="col-6 col-sm-6">
                        <input type="password" className="form-control" name="password" placeholder="Password" onChange={handleChange} value={datiForm.password} required/>
                    </div>
                </div>
                </div>

                <button type="submit" className="btn btn-primary" id="invio">Registrati</button>
            </form>

        </div>

        </>
    )
}