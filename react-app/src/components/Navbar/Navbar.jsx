import '../Navbar/Navbar.css'
import { NavLink } from "react-router-dom"

export default function Navbar(){
    return(
<>

<nav className="navbar navbar-dark bg-dark sticky-top" id="navbarcontainer">
  <form className="form-inline">
  <NavLink
  to="/"
  id="navlink"
  style={({ isActive, isPending, isTransitioning }) => {
    return {
      fontWeight: isActive ? "bold" : "",
      color: isPending ? "red" : "white",
      viewTransitionName: isTransitioning ? "slide" : "",
    };
  }}
>
  Homepage
</NavLink>


  <NavLink
  to="/registrazione"
  id="navlink"
  style={({ isActive, isPending, isTransitioning }) => {
    return {
      fontWeight: isActive ? "bold" : "",
      color: isPending ? "red" : "white",
      viewTransitionName: isTransitioning ? "slide" : "",
    };
  }}
>
  Registrazione
</NavLink>

<NavLink
  to="/login"
  id="navlink"
  style={({ isActive, isPending, isTransitioning }) => {
    return {
      fontWeight: isActive ? "bold" : "",
      color: isPending ? "red" : "white",
      viewTransitionName: isTransitioning ? "slide" : "",
    };
  }}
>
  Login
</NavLink>

  </form>
</nav>
</>
    )
}