import { useState } from "react"
import "../Login/FormLogin.css"
import { useNavigate } from "react-router-dom"
import { useContext } from "react"
import { AuthContext } from "../../contexts/AuthContext"
import { jwtDecode } from "jwt-decode"
import { setToken, setTokenCreationTime, setTtl } from "../../Store/reducers/login"
import { useDispatch, useSelector } from "react-redux"
import { DateTime } from "luxon"
import { reset } from "../../Store/reducers/esempio"

export default function FormLogin(){

//abilito la navigazione ad una root definita
const navigateTo = useNavigate()

let token

//oggetto che restituirà
const [datiUtente, setDatiUtente] = useState([])

//gestione dell'evento per l'inserimento dei dati all'interno dei campi
const handleChange = (e) => {
    const {name, value} = e.target
    //setta il valore inserito in un campo nel form, all'interno dell'oggetto che conterrà tutti i dati (formData)
    setDatiForm({...datiForm, [name]: value})
}



const {datiForm, setDatiForm} = useContext(AuthContext)
//const {datiForm, setDatiForm} = useState([])

//gestione evento per l'invio dei dati
const handleSubmit=(e) => {
    e.preventDefault();
    console.log(datiForm);

    console.log(JSON.stringify(datiForm));
    //setta i valori presi dal form, nell'oggetto che verrà restituito
    setDatiUtente(datiForm)

    setDatiForm(datiForm)
    //invio dei dati al backend
    fetchDataPost();
    
}

//fetch dati post
const fetchDataPost = async () =>{
    const response = await fetch("http://localhost:8080/api/utente/login", {
        mode: "cors",
        method: "POST",
        headers:{
            'Content-Type': 'application/json',
          },
        body: JSON.stringify(datiForm),
    });

  console.log(response.status)

  if(response.status == 200){
    alert("Login avvenuto con successo!");

    //collegamento con il profilo utente
    navigateTo("/profilo");

    // Attendere che la Promise json() si risolva
    const risposta = await response.json();

    console.log(risposta)
    // Ora "risposta" contiene l'oggetto JSON restituito
    token = risposta.token;
    

    //decodifica del token
    const decodedToken = jwtDecode(token);
    console.log(decodedToken)

    //SALVO IN REDUCER
    dispatch(setToken(decodedToken))
    dispatch(setTtl(risposta.ttl))
    dispatch(setTokenCreationTime(risposta.tokenCreationTime))




    //decodedToken è un oggetto contenente tutti i dati collegati al token
    setDatiForm({...datiForm, nome: decodedToken.nome, cognome: decodedToken.cognome})

    //faccio il return dell'oggetto creato
    return JSON.stringify(datiForm);
  }else{
    alert("Si è verificato un errore!");
    //ritorna lo stato del metodo post
    return response.status;
  }
  

}
//con reducer
let tokenReducer=useSelector( (state) =>{
    return state.login
})

//console.log(tokenReducer)

let dispatch = useDispatch();

const checkExpire = () =>{
    setInterval (() =>{
        const ttl = DateTime.fromISO(tokenReducer.ttl)
        const ttlNow = DateTime.now()

        //console.log(ttl)
        //console.log(ttlNow)

        if(ttl>ttlNow){
            console.log("TEMPO ESAURITO")
            dispatch(reset())
            navigateTo("/login")
        }

    }, 900000)
}


    return(
        <>
        
        <div id="formLogin" className="container-sm">
            <h1 id="intestazione">EFFETTUA QUI L'ACCESSO AL TUO PROFILO</h1>
            
            <form onSubmit={handleSubmit}>
                
                <div className="container text-center">
                <div className="row">
                    
                    <div className="col-6 col-sm-6">
                        <label className="form-label">Email</label>
                    </div>
                    <div className="col-6 col-sm-6">
                        <input type="email" className="form-control" name="email" placeholder="Email" onChange={handleChange} value={datiForm.email} required/>
                    </div>

                        <div className="w-100 d-none d-md-block" id="separatore"></div>

                    <div className="col-6 col-sm-6">
                        <label className="form-label">Password</label>
                    </div>
                    <div className="col-6 col-sm-6">
                        <input type="password" className="form-control" name="password" placeholder="Password" onChange={handleChange} value={datiForm.password} required/>
                    </div>
                </div>
                </div>

                <button type="submit" className="btn btn-primary" id="invio" onClick={checkExpire}>Accedi</button>
            </form>

        </div>
        </>
    )
}