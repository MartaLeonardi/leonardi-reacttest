import React from 'react'
import ReactDOM from 'react-dom/client'
import "../node_modules/bootstrap/dist/js/bootstrap.bundle.js"
import "../node_modules/bootstrap/dist/css/bootstrap.css"

import {createBrowserRouter, RouterProvider} from 'react-router-dom'

import Layout from './pages/Layout.jsx'
import Home from './pages/Home.jsx'
import Registrazione from './pages/Registrazione.jsx'
import Login from './pages/Login.jsx'
import Profilo from './pages/Profilo.jsx'
import AuthContextProvider from './contexts/AuthContextProvider.jsx'

import { Provider } from 'react-redux'
import store,{ persistStorage } from './Store/index.jsx'
import { PersistGate } from 'redux-persist/integration/react'

const router = createBrowserRouter([
  {
    element:<AuthContextProvider><Layout/></AuthContextProvider>,
    children:[
      {
        path: "/",

        children:[
          {
            path: "",
            element: <Home/>,
          },
          {
            path: "/registrazione",
            element: <Registrazione/>,
            
          },
          {
            path: "/login",
            element: <Login/>
          },
          {
            path: "/profilo",
            element: <Profilo />
          }
        ]
      },
      
    ]
  }
])

ReactDOM.createRoot(document.getElementById('root')).render(
  <Provider store={store}>
    <PersistGate persistor={persistStorage}>
        <RouterProvider router={router} />
    </PersistGate>
  </Provider>
)
