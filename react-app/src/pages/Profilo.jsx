import { AuthContext } from "../contexts/AuthContext"
import { useContext, useEffect } from "react"
//import store from "../Store/reducers/login"
import { useDispatch, useSelector } from "react-redux"

export default function Profilo(){

    //const {datiForm, setDatiForm} = useContext(AuthContext)

    //useEffect (() => { 
    //    console.log(datiForm)
    //},[] 
    //)

    let dati=useSelector( (state) =>{
        //console.log(state.login.token)
        return state.login.token
    })

    console.log(dati)

    return(
        <>

        <div className="card mb-3">
        <div className="row g-0">
            <div className="col-md-4">
            <img src="" className="img-fluid rounded-start" alt="img utente"/>
            </div>
            <div className="col-md-8">
            <div className="card-body">
                <h5 className="card-title">Dati utente:</h5>
                <p className="card-text">NOME: {dati.nome}</p>
                <p className="card-text">COGNOME: {dati.cognome}</p>
                <p className="card-text">EMAIL: {dati.email}</p>
                <p className="card-text">RUOLO: {dati.ruoli}</p>
            </div>
            </div>
        </div>
        </div>
        </>
    )
}