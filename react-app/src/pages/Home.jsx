
import { increment, incrementByAmount, reset, setUser } from "../Store/reducers/esempio"
import imgHomepage from "../assets/homepageImg.jpg"
import "../components/stylecss.css"
import { useState } from "react"

import { useDispatch, useSelector } from "react-redux"

export default function Home(){


    let contatore=useSelector( (state) =>{
        return state.esempio.value
    })

    console.log(contatore)

    let dispatch = useDispatch ();

    let [valore, setValore] = useState (0);

    //setTimeout(() => {
    //    console.log("ciao")
    //}, 1000);

    //setInterval(() => {
    //    console.log("ciao")
   // }, 1000);

    return(
        <>
        
            <div className="container-fluid" id="homepage">
                <h1>Benvenuto!</h1>
                <img src={imgHomepage} className="img-fluid" alt="img della homepage" />


{/*modifiche con reducer
                <br/>
                <button onClick={() => {dispatch(increment())}}>clicca</button>
                <button onClick={() => {dispatch(reset())}}>reset</button>

                <input type="text" name="count" placeholder="count" onChange={(event) => setValore(event.target.value)} value={valore} required/>
                <button onClick={() => {dispatch(incrementByAmount(Number.parseInt(valore)))}}>incrementa</button>

                <button onClick={() => {dispatch(setUser({token}))}}>incrementa</button>
*/}

            </div>
        
        </>
    )
}