import Navbar from "../components/Navbar/Navbar";
import { useOutlet} from "react-router-dom";
import Footer from "../components/Footer/Footer";

export default function Layout(){
    const outlet = useOutlet()

    return(
        <div>
            <Navbar/>
            {outlet}
            <Footer />
        </div>

    )
}